# Stage 1: import the smtpd-pipe server
FROM registry.git.autistici.org/ai3/tools/smtpd-pipe:master AS smtpd

# Stage 2: build the final container
FROM registry.git.autistici.org/ai3/docker/apache2-base:bookworm
COPY build.sh /tmp/build.sh
COPY ai-lurker-ui.patch /tmp/ai-lurker-ui.patch
COPY conf/ /etc/
COPY --from=smtpd /smtpd /usr/bin/smtpd
RUN /tmp/build.sh && rm /tmp/build.sh /tmp/ai-lurker-ui.patch
COPY logo.png /var/lib/lurker-www-orig/logo.png
