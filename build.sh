#!/bin/sh
#
# Install script for Lurker inside a Docker container.
#

BUILD_PACKAGES="
	patch
"

PACKAGES="
	lurker
"

install_packages() {
    env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
}

die() {
    echo "ERROR: $*" >&2
    exit 1
}

set -x

# Install packages.
apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES} \
    || die "could not install packages"

# Enable the apache modules we need.
a2enmod -q cgid rewrite
a2enconf -q serve-cgi-bin

# No need to enable the default apache vhost.

# Create mountpoint dirs.
mkdir -p /var/lib/lurker

# Lurker installs some package files in mountpoint directories,
# so the original files would disappear "below" the mount. To
# prevent this, we copy the files to a well-known location
# beforehand, and set up links at run-time.
mkdir -p /var/lib/lurker-www-orig
mv /etc/lurker/ui /var/lib/lurker-www-orig/ui
mv /var/lib/lurker/www/index.html /var/lib/lurker-www-orig/index.html

# Apply our UI patch.
(cd /var/lib/lurker-www-orig/ui && patch -p1 </tmp/ai-lurker-ui.patch)

# Clear the existing default config
# (this will be mounted externally).
rm -fr /etc/lurker/*

# Clean up.
apt-get --purge remove ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
