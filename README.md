Docker image for lurker, a mailing list archiver.

The image contains both an Apache server and an incoming SMTP server,
this is simply because the Debian lurker package has a strict dependency
on the apache2 package, so it was more convenient to do it this way
instead of using two separate container images.

Configuration and data storage must be mounted respectively in
*/etc/lurker* and */var/lib/lurker*.

Instead of running a full-blown Postfix instance, this container runs
a very simple [SMTP
server](https://git.autistici.org/ai3/tools/smtpd-pipe) that is only
suitable for internal traffic.

The image expects /var/lib/lurker to be mounted externally with the
right permissions, and to contain the *www* and *data* subdirectories.

## Environment variables

* `DOMAIN`: domain for the HTTP website, minus the *lists* prefix
* `APACHE_PORT`: port for the HTTP service
* `SMTP_PORT`: port for the SMTP service
* `SMTP_EHLO_HOSTNAME`: hostname to use for SMTP EHLO
* `LURKER_ADDR_RX`: regular expression to match SMTP recipients and
  extract the list name from it. It should include a single regexp
  group (the list name). The default handles plus-extension addresses
  using the *lurker* username (`lurker\\+([^@]+)@.*`), but it is
  recommended to extend it to match the expected domain name as well.
* `LURKER_DATA_DIR`: data directory for Lurker (default */var/lib/lurker/data*).

When Lurker is performing maintenance on its database, incoming
SMTP reception is temporarily suspended.

